### ECE651:Homework1

Author: Xiaodan Zhu

#### Overview

This project is a car racing program. It will randomly build 5 engines, car bodies and tires respectively. And you select one from every categories to consist your car. Run it to see whether you can beat the computer.

#### Hierarchy

I create an abastract Class called Component, which represents any component of a car. Class Engine, Body and Tire extend it.


I create an interface roadTestable. Class Sedan implements it since a sedan car can do a road test.

#### Sanpshot of the program

Here is a quick view.
![snapshot](screenShot.png)