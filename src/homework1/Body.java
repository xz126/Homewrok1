package homework1;

public class Body extends Component{
    private int speedUp;
    private int stability;
    private String BodyName;
    public Body(String name) {
      BodyName = name;
    }
    @Override
    public void makeComponent() {
      stability = (int)(Math.random() * 20);
      speedUp = (int) (Math.random() * 20 - Math.random() * stability);
    }
    @Override
    public void printProperties() {
      System.out.println("Speedup of " + BodyName + " is " + speedUp);
      System.out.println("Stability improvement " + BodyName + " is " + stability);
    }
    public double getSpeedUp() {
      return speedUp;
    }
    public void setSpeedUp(int speedUp) {
      this.speedUp = speedUp;
    }
    public int getStability() {
      return stability;
    }
    public void setStability(int stability) {
      this.stability = stability;
    }
    public String getBodyName() {
      return BodyName;
    }
    public void setBodyName(String bodyName) {
      BodyName = bodyName;
    }
}
