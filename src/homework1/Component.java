package homework1;

public abstract class Component {
  public abstract void makeComponent();
  public abstract void printProperties();
}
