package homework1;

public interface RoadTestable {
  public abstract double roadTest(Road road);
}
