package homework1;

public class Engine extends Component{
  private double maxSpeed;
  private int stability;
  private String engineName;
  public Engine(String name) {
    engineName = name;
  }
  @Override
  public void makeComponent() {
    stability = (int)(Math.random() * 100);
    maxSpeed = Math.random() * 20 + 100 - Math.random() * stability;
  }
  @Override
  public void printProperties() {
    System.out.println("MaxSpeed of " + engineName + " is " + maxSpeed);
    System.out.println("Stability of " + engineName + " is " + stability);
  }
  public double getMaxSpeed() {
    return maxSpeed;
  }
  public void setMaxSpeed(double maxSpeed) {
    this.maxSpeed = maxSpeed;
  }
  public int getStability() {
    return stability;
  }
  public void setStability(int stability) {
    this.stability = stability;
  }
  public String getEngineName() {
    return engineName;
  }
  public void setEngineName(String engineName) {
    this.engineName = engineName;
  }
}
