package homework1;

public class Tire extends Component{
  private int speedUp;
  private int stability;
  private String TireName;
  public Tire(String name) {
    TireName = name;
  }
  @Override
  public void makeComponent() {
    stability = (int)(Math.random() * 10);
    speedUp = (int)(Math.random() * 10 - Math.random() * stability);
  }
  @Override
  public void printProperties() {
    System.out.println("Speedup of " + TireName + " is " + speedUp);
    System.out.println("Stability improvement " + TireName + " is " + stability);
  }
  public double getSpeedUp() {
    return speedUp;
  }
  public void setSpeedUp(int speedUp) {
    this.speedUp = speedUp;
  }
  public int getStability() {
    return stability;
  }
  public void setStability(int stability) {
    this.stability = stability;
  }
  public String getTireName() {
    return TireName;
  }
  public void setTireName(String tireName) {
    TireName = tireName;
  }
}
