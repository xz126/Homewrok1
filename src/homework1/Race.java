package homework1;
import java.util.ArrayList;
import java.util.Scanner;


public class Race {

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    System.out.println("Welcome to NEED FOR SPEED (Awsome knock-off version)");
    System.out.println("The Gangster in this city is Devil.");
    System.out.println("You have to win him to live here.");
    System.out.println("Famous car factories are building 5 components of engine, tires and boides...");
    System.out.println("However, you don't know which component is the best.");
    ArrayList<Engine> engines = new ArrayList<Engine>();
    ArrayList<Tire> tires = new ArrayList<Tire>();
    ArrayList<Body> bodies = new ArrayList<Body>();
    for (int i = 0; i < 5; i++) {
      Engine eng = new Engine("TestEngine" + i);
      eng.makeComponent();
      Tire t = new Tire("TestTire" + i);
      t.makeComponent();
      Body b = new Body("TestBody" + i);
      b.makeComponent();
      engines.add(eng);
      tires.add(t);
      bodies.add(b);
    }
    
    Scanner sc = new Scanner(System.in);
    System.out.println("Select an engine for your car (1 - 5): ");
    String numE = sc.nextLine();
    System.out.println("Select a body for your car (1 - 5): ");
    String numB = sc.nextLine();
    System.out.println("Select tires for your car (1 - 5): ");
    String numT = sc.nextLine();
    System.out.println("Name your car: ");
    String name = sc.nextLine();
    sc.close();
    
    System.out.println("Your car has been built!");
    Sedan car1 = new Sedan(name, engines.get(Integer.parseInt(numE) - 1), bodies.get(Integer.parseInt(numB) - 1),tires.get(Integer.parseInt(numT) - 1));
    System.out.println("The max speed of your car is: " + car1.maxSpeed + "\nThe stability of your car is: " + car1.stability);
    System.out.println("Devil's car has been built!");
    Sedan car2 = new Sedan("X", engines.get((int)(Math.random() * 4)), bodies.get((int)(Math.random() * 4)),tires.get((int)(Math.random() * 4)));
    System.out.println("The max speed of Devil's car is: " + car2.maxSpeed + "\nThe stability of Devil's car is: " + car2.stability + "\n");
    System.out.println("Crazy crowd find a terrific road for racing.");
    Road road = new Road(100000, (int)Math.random() * 100);
    double res1 = car1.roadTest(road);
    double res2 = car2.roadTest(road);
    //System.out.println("res1:" + res1 + "\nres2:" + res2);
    if (res1 < res2) {
      System.out.println("You win.");
      System.out.println("\"I will be back.\" Devil stared to you. Then he left.");
    }
    else {
      System.out.println("Devil beated you...Bang!");
    }
  }

}
