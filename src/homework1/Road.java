package homework1;

public class Road {
  private int distance;
  private int jolt;
  public Road(int dis, int jolt) {
    this.distance = dis;
    this.jolt = jolt;
  }
  public int getDistance() {
    return distance;
  }
  public void setDistance(int distance) {
    this.distance = distance;
  }
  public int getJolt() {
    return jolt;
  }
  public void setJolt(int jolt) {
    this.jolt = jolt;
  }
}
