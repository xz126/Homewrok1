package homework1;

public class Sedan implements RoadTestable {
  public int maxSpeed;
  public int stability;
  private Engine engine;
  private Body body;
  private Tire tire;

  public Sedan(String name, Engine engine, Body body, Tire tire) {
    int sum = 0;
    for (int i = 0; i < name.length(); i++) {
      sum += name.charAt(i);
    }
    maxSpeed = sum % 26;
    setEngine(engine);
    setBody(body);
    setTire(tire);
  }
  public void deconstruct() {
    maxSpeed = 0;
    stability = 0;
    engine = null;
    body = null;
    tire = null;
  }
  @Override
  public double roadTest(Road road) {
    // TODO Auto-generated method stub
    if (stability > 100) {
      stability = 100;
    }
    double res = road.getDistance() * 1.0 / (maxSpeed - Math.abs((stability - road.getJolt()) * Math.random()));
    return res;
  }
  
  public Engine getEngine() {
    return engine;
  }
  public void setEngine(Engine engine) {
    this.engine = engine;
    maxSpeed += this.engine.getMaxSpeed();
    stability += this.engine.getStability();
  }
  public Body getBody() {
    return body;
  }
  public void setBody(Body body) {
    this.body = body;
    maxSpeed += this.body.getSpeedUp();
    stability += this.body.getStability();
  }
  public Tire getTire() {
    return tire;
  }
  public void setTire(Tire tire) {
    this.tire = tire;
    maxSpeed += this.tire.getSpeedUp();
    stability += this.body.getStability();
  }
  
}
